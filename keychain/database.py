# This file is part of Keychain.

# Keychain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Keychain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Keychain.  If not, see <http://www.gnu.org/licenses/>.    


import gpgme
import io
import sys
import os

class DBException(Exception):
    def __init__(self,type):
        self.type = type
        super(DBException,self).__init__()


class Passkey(object):
    def __init__(self,title,login,passwd,tags):
        self.title = title
        self.login = login
        self.passwd = passwd
        self.tags = tags

class Database(object):
    def __init__(self,filepath,keyid):
        self.filepath = os.path.abspath(os.path.expanduser(os.path.expandvars(filepath)))
        if not os.path.isfile(self.filepath):
            if not os.path.exists(os.path.dirname(self.filepath)):
                #Path to the file does not exists, we fail
                print(self.filepath+" is not a real path, aborting")
                sys.exit(1)
        self.keyid = keyid
        #Init crypt engine
        self.context = gpgme.Context()
        self.context.armor =True
        try:
            self.key = self.context.get_key(self.keyid)
        except gpgme.GpgmeError:
            print("Could not load key, aborting")
            sys.exit(1)
        #Init db content
        self.keys_by_title = {}
        self.keys_by_tag = {}
        #Token of the database
        self.token = False

    def read_from_file(self):
        if os.path.isfile(self.filepath):
            f = open(self.filepath,"rb")
            decyphered_bytes = io.BytesIO()
            self.context.decrypt(f,decyphered_bytes)
            f.close()
            db_string = decyphered_bytes.getvalue().decode("utf-8")
            #Read the string
            lines = db_string.split("\n")
            #1st line : number of tags
            #2d line : tags
            tags = lines[1].split("|")[:-1]
            #3rd lines : number of key per tag
            #4th line number of keys
            nkey = int(lines[3])
            #Parse all the keys
            for i in range(nkey):
                #number of tags of the key
                #the key
                l = lines[2*i+5].split("|")[:-1]
                title = l[0]
                tags = l[3:]
                key = Passkey(title,l[1],l[2],tags)
                self.keys_by_title[title] = key
                for tag in tags:
                    if tag in self.keys_by_tag:
                        self.keys_by_tag[tag].append(key)
                    else:
                        self.keys_by_tag[tag] = [key]
            #Parsed everything! Yay!
        #If the file does not exists, the database is already empty
        
    def write_to_file(self):
        #Writing is only needed if the databse has been modified
        if self.token:
            db_string = ""
            #1st line is the number of tag
            db_string+=str(len(self.keys_by_tag))
            db_string+="\n"
            #2d line are the tags
            db_string+="|".join(list(self.keys_by_tag.keys()))
            db_string+="|\n"
            #3d line is the number of key per tag
            db_string+="|".join([str(len(x)) for x in self.keys_by_tag.values()])
            db_string+="|\n"
            #4th line is the number of keys
            db_string+=str(len(self.keys_by_title))
            db_string+="\n"
            #The keys
            for key in self.keys_by_title.values():
                #1st line is the number of tags
                db_string+=str(len(key.tags))
                db_string+="\n"
                #2d line is the key
                db_string+=key.title+"|"+key.login+"|"+key.passwd+"|"
                db_string+="|".join(key.tags)
                db_string+="|\n"
            #db_string contains the whole database
            f = open(self.filepath,"wb")
            db = io.BytesIO(db_string.encode())
            self.context.encrypt([self.key],0,db,f,)
            f.close()

    def add_key(self,title,login,passwd,tags):
        #Check if the key title does not exist
        if title in self.keys_by_title:
            raise DBException("title")
        if ("|" in title) or ("|" in login) or ("|" in passwd):
            raise DBException("char")
        for tag in tags:
            if ("|" in tag):
                raise DBException("char")
        #The key is correct
        self.token = True
        key = Passkey(title,login,passwd,tags)
        self.keys_by_title[title] = key
        for tag in tags:
            if tag in self.keys_by_tag:
                self.keys_by_tag[tag].append(key)
            else:
                self.keys_by_tag[tag] = [key]
        return key
    
    def delete_key(self,key):
        title = key.title
        tags = key.tags
        del self.keys_by_title[title]
        for tag in tags:
            if len(self.keys_by_tag[tag]) == 1:
                del self.keys_by_tag[tag]
            else:
                self.keys_by_tag[tag].remove(key)

    def query_title(self,query):
        res = {}
        for title in self.keys_by_title:
            if query.upper() in title.upper():
                res[title] = self.keys_by_title[title]
        return res


        
