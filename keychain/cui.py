# This file is part of Keychain.

# Keychain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Keychain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Keychain.  If not, see <http://www.gnu.org/licenses/>.


import curses
import locale
import keychain.database as database

class Ui(object):
    def __init__(self,std,db):
        locale.setlocale(locale.LC_ALL, '')
        self.code = locale.getpreferredencoding()
        self.std = std
        self.db = db
        self.regen_lists()
        self.sidebar_width = 17
        self.sidebar_height = curses.LINES-7
        curses.curs_set(0)

    def regen_lists(self):
        self.alltitles = sorted(list(self.db.keys_by_title.keys()))
        self.alltags = sorted(list(self.db.keys_by_tag.keys()))
        
    def draw_dialog(self,text):
        win = curses.newwin(4,25,int(curses.LINES/2),30)
        win.addstr(1,1,text)
        win.box()
        win.addstr(2,8,"[y/N]")
        win.refresh()
        c = self.std.getch()
        win.erase()
        win.refresh()
        if c == ord("y"):
            return True
        else:
            return False
    

    def draw_list(self,win,items,offset,index,focus=True):
        #refresh the list
        max_n = self.sidebar_height-2
        win.erase()
        win.box()
        for i in range(min(max_n,len(items))):
            if index == i and focus:
                win.addstr(1+i,1,items[offset+i][0:self.sidebar_width-2],curses.A_REVERSE)
            else:
                win.addstr(1+i,1,items[offset+i][0:self.sidebar_width-2])
        win.refresh()

    def draw_key(self,win,key):
        win.erase()
        win.box()
        win.addstr(1,1,"Title :")
        win.addstr(3,1,"Login :")
        win.addstr(5,1,"Password :")
        win.addstr(7,1,"Tags :")
        if key:
            win.addstr(2,5,key.title)
            win.addstr(4,5,key.login)
            win.addstr(6,5,key.passwd)
            win.addstr(8,5,",".join(key.tags))
        win.refresh()

    def draw_input_field(self,win,length,y,x):
        win.addstr(y,x," "*(length),curses.A_REVERSE)
        win.move(y,x)
        win.refresh()
        curses.echo()
        curses.curs_set(1)
        query = win.getstr().decode(self.code)
        curses.noecho()
        curses.curs_set(0)
        win.addstr(y,x+len(query)," "*(length-len(query)))
        win.refresh()
        return query
        
    def draw_edit_key(self,win,key,index,newtitle,newlogin,newpasswd,newtags):
        win.erase()
        win.box()
        if index == 0:
            win.addstr(1,1,"Title :",curses.A_REVERSE)
        else:
            win.addstr(1,1,"Title :")
        if index == 1:
            win.addstr(4,1,"Login :",curses.A_REVERSE)
        else:
            win.addstr(4,1,"Login :")
        if index == 2:
            win.addstr(7,1,"Password :",curses.A_REVERSE)
        else:
            win.addstr(7,1,"Password :")
        if index == 3:
            win.addstr(10,1,"Tags :",curses.A_REVERSE)
        else:
            win.addstr(10,1,"Tags :")
        win.addstr(2,5,"("+key.title+")")
        win.addstr(5,5,"("+key.login+")")
        win.addstr(8,5,"("+key.passwd+")")
        win.addstr(11,5,"("+",".join(key.tags)+")")
        win.addstr(3,5,newtitle)
        win.addstr(6,5,newlogin)
        win.addstr(9,5,newpasswd)        
        win.addstr(12,5,",".join(newtags))
        win.refresh()

    def edit_loop(self,win,key):
        index = 0
        nkey = key
        newtitle = ""
        newlogin = ""
        newpasswd = ""
        newtags = []
        self.draw_edit_key(win,key,index,newtitle,newlogin,newpasswd,newtags)
        self.std.addstr(1,2,"F3 : Save and stop editting | q : discard and stop editting                 ")
        self.std.addstr(curses.LINES-2,2,"tags must be separated by ',' (without spaces)               ")
        while True:
            c = self.std.getch()
            if c == curses.KEY_F3:
                #Empty records are equals to the old ones
              
                #Check corectness
                check = True
                if newtitle != key.title and newtitle in self.db.keys_by_title:
                    self.draw_dialog("The title already exists")
                    check = False
                if ("|" in newtitle) or ("|" in newlogin) or ("|" in newpasswd):
                    self.draw_dialog("'|' found in a field")
                    check = False
                for tag in newtags:
                    if ("|" in tag):
                        self.draw_dialog("'|' found in a field")
                        check = False
                #No need to do anything if all fields are blank
                if (not newtitle) and (not newlogin) and (not newpasswd) and (not newtags):
                    self.draw_dialog("Nothing changed")
                    check = False
                if check:
                    if not newtitle:
                        newtitle = key.title
                    if not newlogin:
                        newlogin = key.login
                    if not newpasswd:
                        newpasswd = key.passwd
                    if not newtags:
                        newtags = key.tags
                    #Delete old key
                    self.db.delete_key(key)
                    #Add new key
                    nkey = self.db.add_key(newtitle,newlogin,newpasswd,newtags)
                    break
            if c == curses.KEY_UP:
                if index > 0:
                    index-=1
                self.draw_edit_key(win,key,index,newtitle,newlogin,newpasswd,newtags)
            if c == curses.KEY_DOWN:
                if index < 3:
                    index+=1
                self.draw_edit_key(win,key,index,newtitle,newlogin,newpasswd,newtags)
            if c == ord("q"):
                break
            if c == 10:
                if index == 0:
                    newtitle = self.draw_input_field(win,15,3,5)
                elif index == 1:
                    newlogin = self.draw_input_field(win,15,6,5)
                elif index == 2:
                    newpasswd = self.draw_input_field(win,15,9,5)
                elif index == 3:
                    newtags = self.draw_input_field(win,25,12,5).split(",")
        self.std.addstr(1,2,"F1 : Browse titles | F2 : Browse tags | F3 : Edit key | F4 : Add key")
        self.std.addstr(curses.LINES-2,2,"k : delete key | s : search | q : quit")
        return nkey
        
            
    def title(self):
        index = 0 #index of pointed title
        offset = 0 #Offset of the shown titles
        key = None #Key curently displayed
        titles = self.alltitles
        self.std.erase()
        title_win = curses.newwin(self.sidebar_height,self.sidebar_width,5,2)
        search_win = curses.newwin(3,self.sidebar_width,2,2)
        search_win.box()
        self.std.addstr(3,3+self.sidebar_width,"search")
        key_win = curses.newwin(self.sidebar_height,curses.COLS-self.sidebar_width-4,5,self.sidebar_width+3)
        self.std.addstr(1,2,"F1 : Browse titles | F2 : Browse tags | F3 : Edit key | F4 : Add key")
        self.std.addstr(curses.LINES-2,2,"k : delete key | s : search | q : quit")
        self.std.refresh()
        self.draw_list(title_win,titles,offset,index)
        self.draw_key(key_win,None)
        search_win.refresh()
        while True:
            c = self.std.getch()
            if c == ord("q"):
                return 3
            elif c == curses.KEY_F2:
                return 1
            elif c == curses.KEY_F3:
                if key:
                    nkey = self.edit_loop(key_win,key)
                    if nkey:
                        key = nkey
                        index = 0
                        offset = 0
                        self.regen_lists()
                        titles = self.alltitles
                        self.draw_list(title_win,titles,offset,index)
                    self.draw_key(key_win,key)
            elif c == curses.KEY_F4:
                return 2
            elif c == curses.KEY_UP:
                if (offset >0) and (index == 0):
                    offset-=1
                elif index > 0:
                    index-=1
                self.draw_list(title_win,titles,offset,index)
            elif c == curses.KEY_DOWN:
                if (index == (self.sidebar_height-3)) and (offset+index < len(titles)-1):
                    offset+=1
                elif index < min(len(titles)-1,self.sidebar_height-3):
                    index+=1
                self.draw_list(title_win,titles,offset,index)
            elif c == ord("s"):
                query = self.draw_input_field(search_win,self.sidebar_width-2,1,1)
                if query:
                    titles = list(self.db.query_title(query))
                else:
                    titles = self.alltitles
                index = 0
                offset = 0
                self.draw_list(title_win,titles,offset,index)
            elif c == 10:
                if len(titles):
                    key = self.db.keys_by_title[titles[index+offset]]
                    self.draw_key(key_win,key)
            elif c == ord("k"):
                if key:
                    if self.draw_dialog("Confirm deletion?"):
                        self.db.delete_key(key)
                        key = None
                        index = 0
                        offset = 0
                        self.regen_lists()
                        titles = self.alltitles
                        self.draw_list(title_win,titles,offset,index)
                        self.draw_key(key_win,key)

    def draw_addkey(self,win,index,title,login,passwd,tags):
        win.erase()
        win.box()
        if index == 0:
            win.addstr(1,1,"Title :",curses.A_REVERSE)
        else:
            win.addstr(1,1,"Title :")
        if index == 1:
            win.addstr(3,1,"Login :",curses.A_REVERSE)
        else:
            win.addstr(3,1,"Login :")
        if index == 2:
            win.addstr(5,1,"Password :",curses.A_REVERSE)
        else:
            win.addstr(5,1,"Password :")
        if index == 3:
            win.addstr(7,1,"Tags :",curses.A_REVERSE)
        else:
            win.addstr(7,1,"Tags :")
        win.addstr(2,5,title)
        win.addstr(4,5,login)
        win.addstr(6,5,passwd)        
        win.addstr(8,5,",".join(tags))
        win.refresh()

    def add(self):
        index = 0
        title = ""
        login = ""
        passwd = ""
        tags = []
        self.std.erase()
        win = curses.newwin(curses.LINES-4,curses.COLS-4,2,2)
        self.std.addstr(1,2,"F1 : Browse titles | F2 : Browse tags | F4 : Save new key            ")
        self.std.addstr(curses.LINES-2,2,"q : quit | tags must be separated by ',' (without spaces)  ")
        self.std.refresh()
        self.draw_addkey(win,index,title,login,passwd,tags)
        while True:
            c = self.std.getch()
            if c == curses.KEY_F1:
                return 0
            elif c == curses.KEY_F2:
                return 1
            elif c == curses.KEY_F4:
                #Addkey rolls the checks
                if not title:
                    self.draw_dialog("A least a title is needed")
                elif not tags:
                    self.draw_dialog("A least one tag is needed")
                else:
                    try:
                        self.db.add_key(title,login,passwd,tags)
                        self.regen_lists()
                        return 0
                    except database.DBException as e:
                        if e.type == "title":
                            self.draw_dialog("This title already exists")
                        elif e.type == "char":
                            self.draw_dialog("'|' found in a field")
                        else:
                            self.draw_dialog("Unknown error")
            elif c == curses.KEY_UP:
                if index > 0:
                    index-=1
                self.draw_addkey(win,index,title,login,passwd,tags)
            elif c == curses.KEY_DOWN:
                if index < 3:
                    index+=1
                self.draw_addkey(win,index,title,login,passwd,tags)
            elif c == ord("q"):
                return 3
            elif c == 10:
                if index == 0:
                    title = self.draw_input_field(win,20,2,5)
                elif index == 1:
                    login = self.draw_input_field(win,20,4,5)
                elif index == 2:
                    passwd = self.draw_input_field(win,20,6,5)
                elif index == 3:
                    tags = self.draw_input_field(win,20,8,5).split(",")

    def tag(self):
        indexTags = 0 #index of pointed tag
        indexTitles = 0 #index of titles
        offsetTags = 0 #Offset of the shown tags
        offsetTitles = 0 #offset of the shown titles
        browseTags = True #True if we browse tags, False if we browse titles
        key = None #Key curently displayed
        titles = self.alltitles
        tags = self.alltags
        self.std.erase()
        tag_win = curses.newwin(self.sidebar_height,12,5,2)
        title_win = curses.newwin(self.sidebar_height,self.sidebar_width,5,16)
        search_win = curses.newwin(3,self.sidebar_width,2,2)
        search_win.box()
        self.std.addstr(3,3+self.sidebar_width,"search")
        key_win = curses.newwin(self.sidebar_height,curses.COLS-self.sidebar_width-19,5,self.sidebar_width+18)
        self.std.addstr(1,2,"F1 : Browse titles | F2 : Browse tags | F3 : Edit key | F4 : Add key")
        self.std.addstr(curses.LINES-2,2,"tab : navigate menus | k : delete key | s : search | q : quit")
        self.std.refresh()
        self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
        self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
        self.draw_key(key_win,None)
        search_win.refresh()
        while True:
            c = self.std.getch()
            if c == curses.KEY_F1:
                return 0
            elif c == curses.KEY_F3:
                if key:
                    nkey = self.edit_loop(key_win,key)
                    if nkey:
                        key = nkey
                        index = 0
                        offset = 0
                        self.regen_lists()
                        titles = self.alltitles
                        tags = self.alltags
                        self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                        self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
                    self.draw_key(key_win,key)
            elif c == curses.KEY_F4:
                return 2
            elif c == ord('q'):
                return 3
            elif c == ord('k'):
                if key:
                    if self.draw_dialog("Confirm deletion?"):
                        self.db.delete_key(key)
                        key = None
                        indexTags = 0
                        offsetTags = 0
                        indexTitles = 0
                        offsetTitles = 0
                        self.regen_lists()
                        titles = self.alltitles
                        tags = self.alltags
                        self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                        self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
                        self.draw_key(key_win,key)
            elif c == 10:
                if browseTags:
                    if len(tags):
                        tag = tags[indexTags+offsetTags]
                        titles = [k.title for k in self.db.keys_by_tag[tag]]
                        self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
                else:
                    if len(titles):
                        key = self.db.keys_by_title[titles[indexTitles+offsetTitles]]
                        self.draw_key(key_win,key)
            elif c == ord('s'):
                query = self.draw_input_field(search_win,self.sidebar_width-2,1,1)
                if query:
                    if query in self.db.keys_by_tag.keys():
                        titles = [k.title for k in self.db.keys_by_tag[query]]
                        tags = [query]
                    else:
                        titles = self.alltitles
                        tags = self.alltags
                else:
                    titles = self.alltitles
                    tags = self.alltags
                indexTags = 0
                offsetTags = 0
                indexTitles = 0
                offsetTitles = 0
                self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
            elif c == curses.KEY_UP:
                if browseTags:
                    if (offsetTags >0) and (indexTags == 0):
                        offsetTags-=1
                    elif indexTags > 0:
                        indexTags-=1
                    self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                else:
                    if (offsetTitles >0) and (indexTitles == 0):
                        offsetTitles-=1
                    elif indexTitles > 0:
                        indexTitles-=1
                    self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
            elif c == curses.KEY_DOWN:
                if browseTags:
                    if (indexTags == (self.sidebar_height-3)) and (offsetTags+indexTags < len(tags)-1):
                        offsetTags+=1
                    elif indexTags < min(len(tags)-1,self.sidebar_height-3):
                        indexTags+=1
                    self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                else:
                    if (indexTitles == (self.sidebar_height-3)) and (offsetTitles+indexTitles < len(titles)-1):
                        offsetTitles+=1
                    elif indexTitles < min(len(titles)-1,self.sidebar_height-3):
                        indexTitles+=1
                    self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))
            elif c == 9: #tab
                browseTags = not browseTags
                self.draw_list(tag_win,tags,offsetTags,indexTags,focus=browseTags)
                self.draw_list(title_win,titles,offsetTitles,indexTitles,focus=(not browseTags))

        

def cui_loop(db):
    def loop(std):
        ui = Ui(std,db)
        switch = 0
        while True:
            if switch == 0:
                switch = ui.title()
            elif switch == 1:
                switch = ui.tag()
            elif switch == 2:
                switch = ui.add()
            elif switch == 3:
                return
    return loop
            
        
    




