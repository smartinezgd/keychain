Keychain
========

A small python program for managing a crypted password database.

This software is distributed under the Gnu Public Licence (v3) and
provided AS IS, in hope that it will be usefull but WITHOUT ANY
WARRANTY.


Installing
----------

To install Keychain, run

    python setup.py --prefix=<path where your PYTHONPATH points at>

Configuration file
------------------

By default, Keychain will look for the configuration file
`$HOME/.keychain/config`. You can use option `-c` to specify another
path.

That configuration file should contain at least the following keys :

    [database]
    path = <absolute path to the database>
    key_id = id of the public key to use for crypting the database

Database
--------

If you have no database file, Keychain will produce one as soone as
you register a new password. If you already have a database file, you
can specify the path to it in the configuration file or with the `-d`
option.





