from distutils.core import setup

setup(
    name = "Keychain",
    version="1.0",
    author="garrik",
    author_email="garrik@garrik.info",
    packages=["keychain"],
    scripts=["bin/keychain"],
)
